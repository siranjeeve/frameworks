package stepdef.org;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Stepdefdemoqa {
	public WebDriver driver;

	@Given("^I am demoqa website$")
	public void i_am_demoqa_website()  {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Siranjeeve\\eclipse-workspace\\JDBC\\driver\\chromedriver.exe");
		 driver=new ChromeDriver();
        driver.get("http://demoqa.com/registration/");	    
	}

	@When("^I enter firstname & lastname$")
	public void i_enter_firstname_lastname() throws Exception {
		driver.findElement(By.id("name_3_firstname")).sendKeys("siranj");
		driver.findElement(By.id("name_3_lastname")).sendKeys("last");
	    
        throw new PendingException();
	}

	@Then("^I validate the firstname & lastname$")
	public void i_validate_the_firstname_lastname() throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}



}
