package stepdef.org;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Stepdefoutline {
	public WebDriver driver;

	@Given("^i am in demoqa website$")
	public void i_am_in_demoqa_website() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Siranjeeve\\eclipse-workspace\\JDBC\\driver\\chromedriver.exe");
		 driver=new ChromeDriver();
       driver.get("http://demoqa.com/registration/");	    
	}

	@When("^i enter \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_and(String first, String last) throws Exception {
		driver.findElement(By.id("name_3_firstname")).sendKeys(first);
		driver.findElement(By.id("name_3_lastname")).sendKeys(last);	    
	}

	@Then("^I verify the \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_verify_the_and(String arg1, String arg2) throws Exception {
	    // Write code here that turns the phrase above into concrete actions
	}



}
