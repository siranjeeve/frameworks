#Author: your.email@your.domain.com

@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @tag2
  Scenario Outline: Title of your scenario outline
    Given i am in demoqa website
    When i enter "<firstname>" and "<lastname>"
    Then I verify the "<firstname>" and "<lastname>"

    Examples: 
      | firstname  | lastname | 
      | siranj |  yuvi |
      | anand |     r |
